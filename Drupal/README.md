# CWE: Improper Input Validation
### CVE: 2018-7600
Exploit-db: https://www.exploit-db.com/exploits/44448

Description: Drupal < 8.3.9 / < 8.4.6 / < 8.5.1 - 'Drupalgeddon2' Remote Code Execution due to incorrect form parameters validation


### How to run it locally

    docker-compose up -d
    
Now your vulnerable app is accessible at http://localhost/

### Running the exploit

Install [ruby](https://www.ruby-lang.org/en/documentation/installation/) to run the script.
    
    (Debian or Ubuntu) sudo apt-get install ruby-full
    (CentOS, Fedora, or RHEL) sudo yum install ruby
    
Then, install the required gem

    sudo gem install highline

Run the script

    ruby ./exploit/drupalgeddon2.rb http://localhost:7000/