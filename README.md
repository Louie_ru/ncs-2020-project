# NCS Project

### How to run

    - *cd* to any vulnerability
    - *docker-compose up*

### More details

This is Network and Cyber Security assignment 2020 Innopolis University. Technical details of vulnerabilities are described in PDF report.

# Links to docker hub:
    - https://hub.docker.com/r/louieru/ghostscript
    - https://hub.docker.com/r/louieru/heartbleed
    - https://hub.docker.com/r/slavagoreev/ncs-drupal
    - https://hub.docker.com/r/srbgd/tcpdump
    - https://hub.docker.com/r/slavagoreev/uwsgi-directory-traversal
    - https://hub.docker.com/r/srbgd/curl
