# curl

**DockerHub**: https://hub.docker.com/r/srbgd/curl

**Exploit**: https://www.exploit-db.com/exploits/24487

**CVE**: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0249 (CVE-2013-0249)

**CWE**: Improper Restriction of Operations within the Bounds of a Memory Buffer (CWE-119)

**CVSS**: 7.5

Run it with `docker-compose up`

In order to exploit the vulnurability run `sudo docker exec -it curl curl pop3://x:x@localhost/`

Expected behaivor is POP3 QUIT message

Actual behaivor is crashing of the program before an irrilevant error message is fully flushed.

More details in report and demo video.