# tcpdump

**DockerHub**: https://hub.docker.com/r/srbgd/tcpdump

**Exploit**: http://www.exploit-db.com/exploits/35359

**CVE**: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-8768 (CVE-2014-8768)

**CWE**: Integer Underflow (Wrap or Wraparound) (CWE-191)

**CVSS**: 5.0

Run it with `docker-compose up`

In order to exploit the vulnurability run `sudo docker exec -it tcpdump python exploit.py`

Expected behaivor is printing the content of the package on the screen

Actual behaivor is printing the content of memory of the process and its crashing.

More details in report and demo video.
